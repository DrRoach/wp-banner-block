/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#useblockprops
 */
import { useBlockProps, InspectorControls } from '@wordpress/block-editor';
import { PanelBody, TextControl, ColorPicker, FontSizePicker } from '@wordpress/components';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#edit
 *
 * @return {Element} Element to render.
 */

function Button({ text, styles }) {
    if (typeof text == "undefined" || text == "") {
        // No text set so don't render anything
        return;
    }

    console.log(styles);

    return <button style={styles}>{ text }</button>;
}

export default function Edit({ attributes, setAttributes }) {
    // Set the attributes
    // Banner background colour
    const bannerBackgroundColor = attributes.bannerBackgroundColour || "#000";

    // Heading
    const headingColor = attributes.bannerHeadingColour || "#FFF";
    const headingText = attributes.bannerHeadingText || "Heading";
    const headingTextSize = attributes.bannerHeadingTextSize;

    // Sub heading
    const subHeadingColor = attributes.bannerSubHeadingColour;
    const subHeadingText = attributes.bannerSubHeadingText;
    const subHeadingTextSize = attributes.bannerSubHeadingTextSize;

    // Button
    const buttonBackgroundColor = attributes.bannerButtonBackgroundColour;
    const buttonText = attributes.bannerButtonText;
    const buttonTextSize = attributes.bannerButtonTextSize;
    const buttonTextColor = attributes.bannerButtonTextColour;

    // Set the background colour on useBlockProps() so that block is selectable in the editor
    const blockProps = useBlockProps()
    blockProps.style = {
        backgroundColor: bannerBackgroundColor,
        padding: "5rem 10rem 5rem 10rem"
    };

    // Set heading and sub heading styles
    const headingStyles = {
        color: headingColor,
        fontWeight: "bold",
        width: "50%",
        fontSize: headingTextSize
    };
    const subHeadingStyles = {
        color: subHeadingColor,
        width: "50%",
        fontSize: subHeadingTextSize
    };
    const buttonStyles = {
        color: buttonTextColor,
        fontSize: buttonTextSize,
        backgroundColor: buttonBackgroundColor,
        border: 0,
        padding: "20px 50px 20px 50px",
        marginTop: "20px"
    };

	return (
        <>
            <InspectorControls>
                <PanelBody title={ __( 'Banner Background', 'banner-block' ) }>
                    <ColorPicker
                        label={ __(
                            "Banner Background Colour",
                            "banner-block"
                        ) }
                        color={ bannerBackgroundColor }
                        defaultValue="#FF0000"
                        onChange={ (value) => 
                            setAttributes({ bannerBackgroundColour: value })
                        }
                    />
                </PanelBody>
                <PanelBody title={ __( 'Banner Heading', 'banner-block' ) }>
                    <TextControl
                        label={ __(
                            "Text",
                            "banner-block"
                        ) }
                        value={ headingText || '' }
                        onChange={ (value) =>
                            setAttributes({ bannerHeadingText: value })
                        }
                    />
                    <ColorPicker
                        color={ headingColor }
                        defaultValue="#FF0000"
                        onChange={ (value) =>
                            setAttributes({ bannerHeadingColour: value })
                        }
                    />
                    <FontSizePicker
                        fontSizes={[
                            {
                                name: 'Small',
                                size: 54,
                                slug: 'small'
                            },
                            {
                                name: 'Normal',
                                size: 66,
                                slug: 'normal'
                            },
                            {
                                name: 'Big',
                                size: 78,
                                slug: 'big'
                            }
                        ]}
                        units={[
                            'px',
                            'em',
                            'rem'
                        ]}
                        value={66}
                        onChange={ (value) =>
                            setAttributes({ bannerHeadingTextSize: value })
                        }
                    />
                </PanelBody>
                <PanelBody title={ __( 'Banner Sub Heading', 'banner-block' ) }>
                    <TextControl
                        label={ __(
                            "Text",
                            "banner-block"
                        ) }
                        value={ subHeadingText || '' }
                        onChange={ (value) =>
                            setAttributes({ bannerSubHeadingText: value })
                        }
                    />
                    <ColorPicker
                        color={ subHeadingColor }
                        defaultValue="#FF0000"
                        onChange={ (value) =>
                            setAttributes({ bannerSubHeadingColour: value })
                        }
                    />
                    <FontSizePicker
                        fontSizes={[
                            {
                                name: 'Small',
                                size: 14,
                                slug: 'small'
                            },
                            {
                                name: 'Normal',
                                size: 20,
                                slug: 'normal'
                            },
                            {
                                name: 'Big',
                                size: 24,
                                slug: 'big'
                            }
                        ]}
                        units={[
                            'px',
                            'em',
                            'rem'
                        ]}
                        value={20}
                        onChange={ (value) =>
                            setAttributes({ bannerSubHeadingTextSize: value })
                        }
                    />
                </PanelBody>
                <PanelBody title={ __( 'Banner Button', 'banner-block' ) }>
                    <TextControl
                        label={ __(
                            "Text",
                            "banner-block"
                        ) }
                        value={ buttonText || '' }
                        onChange={ (value) =>
                            setAttributes({ bannerButtonText: value })
                        }
                    />
                    <ColorPicker
                        color={ buttonBackgroundColor }
                        defaultValue="#000"
                        onChange={ (value) =>
                            setAttributes({ bannerButtonBackgroundColour: value })
                        }
                    />
                    <ColorPicker
                        color={ buttonTextColor }
                        defaultValue="#FFF"
                        onChange={ (value) =>
                            setAttributes({ bannerButtonTextColour: value })
                        }
                    />
                    <FontSizePicker
                        fontSizes={[
                            {
                                name: 'Small',
                                size: 14,
                                slug: 'small'
                            },
                            {
                                name: 'Normal',
                                size: 20,
                                slug: 'normal'
                            },
                            {
                                name: 'Big',
                                size: 24,
                                slug: 'big'
                            }
                        ]}
                        units={[
                            'px',
                            'em',
                            'rem'
                        ]}
                        value={20}
                        onChange={ (value) =>
                            setAttributes({ bannerButtonTextSize: value })
                        }
                    />
                </PanelBody>
            </InspectorControls>
            <div { ...blockProps }>
                <h1 style={headingStyles}>
                    { __( headingText, 'banner-block' ) }
                </h1>
                <p style={subHeadingStyles}>
                    { __( subHeadingText, 'banner-block' ) }
                </p>
                <Button text={buttonText} styles={buttonStyles} />
            </div>
        </>
	);
}
