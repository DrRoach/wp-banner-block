<?php
/**
 * @see https://github.com/WordPress/gutenberg/blob/trunk/docs/reference-guides/block-api/block-metadata.md#render
 */
?>
<div style="background: <?php esc_html_e( (!empty($attributes['bannerBackgroundColour'])) ? $attributes['bannerBackgroundColour']
 : '#000' ) ?>; padding: 5rem 10rem 5rem 10rem;">
    <h1 style="color: <?php esc_html_e( (!empty($attributes['bannerHeadingColour'])) ? $attributes['bannerHeadingColour'] : '#FFF' ) ?>; font-size: <?php esc_html_e( (!empty($attributes['bannerHeadingTextSize'])) ? $attributes['bannerHeadingTextSize'] : '66' ) ?>px; width: 50%; font-weight: bold;">
        <?php esc_html_e( $attributes['bannerHeadingText'] ?? 'Heading', 'banner-block' ); ?>
    </h1>
    <?php if (!empty($attributes['bannerSubHeadingText'])): ?>
    <p style="color: <?php esc_html_e( (!empty($attributes['bannerSubHeadingColour'])) ? $attributes['bannerSubHeadingColour'] : '#000' ) ?>; font-size: <?php esc_html_e( (!empty($attributes['bannerSubHeadingTextSize'])) ? $attributes['bannerSubHeadingTextSize'] : "20" ) ?>px; width: 50%;">
        <?php esc_html_e( $attributes['bannerSubHeadingText'], 'banner-block' ); ?>
    </p>
    <?php endif; ?>
    <?php if (!empty($attributes['bannerButtonText'])): ?>
        <button style="background: <?php esc_html_e( (!empty($attributes['bannerButtonBackgroundColour'])) ? $attributes['bannerButtonBackgroundColour'] : '#000' ) ?>; border: 0; margin-top: 20px; padding: 20px 50px 20px 50px; color: <?php esc_html_e( (!empty($attributes['bannerButtonTextColour'])) ? $attributes['bannerButtonTextColour'] : '#FFF' ) ?>; font-size: <?php esc_html_e( (!empty($attributes['bannerButtonTextSize'])) ? $attributes['bannerButtonTextSize'] : '20') ?>px;">
            <?php esc_html_e( $attributes['bannerButtonText'] ?? 'Button' ) ?>
        </button>
    <?php endif; ?>
</div>
